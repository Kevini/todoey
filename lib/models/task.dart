import 'package:flutter/material.dart';

class Task extends ChangeNotifier {
  
  String title;
  bool isCompleted;

  Task({this.title, this.isCompleted = false,});


  void toggleDone() {
    isCompleted =! isCompleted;
  }


}
